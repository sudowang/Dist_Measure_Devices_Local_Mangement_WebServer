//这个模块用来切换生产环境和调试环境的index.html文件.
//根据不同的环境,复制indexDev或indexPro为index.html.
//index.html文件本身是不需要更改的.
module.exports = (function() {
    var os = require('os');
    var fs = require('fs');

    var ifaces = os.networkInterfaces();
    var ip = [];
    (function() {
        for (var dev in ifaces) {
            var alias = 0;
            ifaces[dev].forEach(function(details) {
                if (details.family == 'IPv4') {
                    ip.push(details.address);
                }
            });
        }
    })();
    
    var devFile = __dirname + '/public/indexDev.html';
    var proFile = __dirname + '/public/indexPro.html';
    var destFile = __dirname + '/public/index.html';

    function copy(src, dest) {
        fs.createReadStream(src).pipe(fs.createWriteStream(dest));
    }

    if (ip[0].toString().indexOf('192') >= 0) {
        console.log('develop enviroment! use public/indexDev.html file!');
        copy(devFile, destFile);
    } else {
        console.log('production enviroment! use public/indexPro.html file!');
        copy(proFile, destFile);
    }
    return true;
})();
