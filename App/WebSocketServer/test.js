var redis = require('redis');

var redisClientSubscriber = redis.createClient();
var redisClientPublisher = redis.createClient();
var redisNameSpace = "DistMeasureManage:FromController"; //redis中的命名空间



function send() {
    var data = {
        Command: "Measure",
        Para: {
            Device: Math.round(Math.random() * 10),
            Value: 5 + parseFloat(Math.random().toFixed(3))
        }
    };
    var dataStr = JSON.stringify(data);
    redisClientPublisher.publish(redisNameSpace, dataStr);
}

function send_toggle(v) {
    var data = {
        Command: "ToggleLaser",
        Para: {
            Device: Math.round(Math.random() * 10),
            Toggle: v
        }
    };
    var dataStr = JSON.stringify(data);
    redisClientPublisher.publish(redisNameSpace, dataStr);
}

function Measure() {
    setTimeout(function() {
        send();
        Measure();
    }, 2000);
}


Measure();
// send_toggle(1);
// setTimeout(function() {
//     send_toggle(0);
// }, 5000);
