module.exports = function(app) {

    var monitorPointsDictionary = require('./routes/monitorPointsDictionary.js');
    app.use('/monitorPointsDictionary', monitorPointsDictionary);

};
