define(['./../service/showMsg', './../directive/toggleButton', './../directive/digitInput', './../directive/webSocketHandler', './../model/Data'], function() {
    var app = angular.module('App.home');
    app.controller('homeCtrl', ['$scope', '$rootScope', '$location', '$http', 'showMsg', 'Data',
        function($scope, $rootScope, $location, $http, showMsg, Data) {
            function initialHeight() {
                var height = document.documentElement.clientHeight;
                var h1 = $('#digit-panel').height();
                var h2 = $('#menu-panel').height();
                $('#ng-scope').height(height);
                $('#display-panel').height(height - h1 - h2);
            }
            initialHeight(); //设置Display面板的高度
            $scope.laser_on = false; //激光指示,是否常量
            $scope.front_face = true; //显示 正面 or 反面
            $scope.single = true; //是否处于单选
            $scope.startDevice = 1; //起始设备编号
            $scope.endDevice = 44; //终止设备编号
            $scope.failedDevices = [];


            $scope.data1 = new Data();
            $scope.data2 = new Data();

            var _old_Device_no = -1;



            $scope.beforeChangeDevice = function() {
                if ($scope.laser_on) {
                    showMsg("先关闭激光!", "info");
                    $scope.startDevice = _old_Device_no;
                }
            };

            //单选
            $scope.singleCheck = function() {
                $scope.single = true;
            };
            //多选
            $scope.multipleCheck = function() {
                if ($scope.laser_on) {
                    showMsg("先关闭激光!", "info");
                    return;
                } else {
                    $scope.single = false;
                }
            };

            //发送Sokcet命令到后台服务器
            function send_socket_command(cmdObj) {
                if ($rootScope.Socket) {
                    $rootScope.Socket.emit("DistMeasureManage", cmdObj);
                }
            }
            //收到测量命令
            $scope.on_Measure = function(para) {
                $scope.$apply(function() {
                    if (para.Value === 0) {
                        //观测值为0,则添加不成功的列表
                        $scope.failedDevices.push(para.Device);
                    }
                    if ($scope.front_face) {
                        $scope.data2 = new Data(para);
                    } else {
                        $scope.data1 = new Data(para);
                    }
                    $scope.front_face = !$scope.front_face;
                });
            };

            var unWatch_startDevice = null;
            //收到切换激光的返回结果
            $scope.on_ToggleLaser = function(para) {
                if (para.Toggle === 1) {
                    //在打开激光的时候开始监视startDevice
                    unWatch_startDevice = $scope.$watch('startDevice', function(newV, oldV) {
                        if (newV != _old_Device_no) {
                            _old_Device_no = oldV;
                        }
                    });
                } else {
                    if (unWatch_startDevice) {
                        unWatch_startDevice();
                    }
                }
                $scope.$apply(function() {
                    $scope.laser_on = para.Toggle === 1 ? true : false;
                });
            };



            $scope.measure = function() {
                if ($scope.laser_on) {
                    showMsg("先关闭激光!", "info");
                    return;
                }
                if ($scope.single) {
                    measure_Single();
                } else {
                    measure_Group();
                }
                //清空不成功的设备列表
                $scope.failedDevices = [];
            };

            //单个测量
            function measure_Single(arguments) {
                var number = parseInt($scope.startDevice);
                if (number) {
                    var cmd = {
                        Command: "Measure",
                        Para: number
                    };
                    send_socket_command(cmd);
                } else {
                    showMsg("仪器编号不正确!", "alert");
                }
            }
            //多个测量
            function measure_Group() {
                var number1 = parseInt($scope.startDevice);
                var number2 = parseInt($scope.endDevice);
                if (number1 && number2) {
                    var cmd = {
                        Command: "MeasureGroup",
                        Para: {
                            StartDevice: number1,
                            EndDevice: number2
                        }
                    };
                    send_socket_command(cmd);
                } else {
                    showMsg("仪器编号不正确!", "alert");
                }
            };
            //打开激光
            $scope.toggle_laser = function() {
                if ($scope.single) {
                    var number = parseInt($scope.startDevice);
                    if (number) {
                        var cmd = {
                            Command: "ToggleLaser",
                            Para: {
                                Device: number,
                                Toggle: $scope.laser_on ? 0 : 1
                            }
                        };
                        send_socket_command(cmd);
                    } else {
                        showMsg("仪器编号不正确!", "alert");
                    }
                }
            };
            //停止测量
            $scope.stop_measure = function() {
                if ($scope.laser_on) {
                    showMsg("先关闭激光!", "info");
                    return;
                } else {
                    var cmd = {
                        Command: "StopMeasure",
                        Para: {}
                    };
                    send_socket_command(cmd);
                }
            };
            //接收停止测量的指令
            $scope.on_StopMeasure = function(para) {
                if (para === 1) {
                    showMsg("停止测量成功!", "info");
                } else {
                    showMsg("停止测量失败!", "note");
                }
            };
            //导出
            $scope.export = function() {
                var cmd = {
                    Command: "Export",
                    Para: {}
                };
                send_socket_command(cmd);
            };
            //接收导出结果
            $scope.on_Export = function(para) {
                if (para === "") {
                    showMsg("导出成功", "info");
                } else if (para === "Download") {
                    if (navigator.userAgent.indexOf("iPhone") > -1 || navigator.userAgent.indexOf("iPad") > -1)
                    {
                        // iOS Safari
                        showMsg("导出失败:没有可用的USB设备!", "note");
                    } else {
                        // Other browser
                        location.href = "测距仪数据.xlsx";
                    }
                } else {
                    showMsg("导出失败:" + para, "note");
                }
            }

            //接收出错的消息
            $scope.on_Err = function(para) {
                showMsg(para, "alert");
            };








        }
    ]);
});
