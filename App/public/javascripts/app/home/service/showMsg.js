define(['./../initial'], function() {
    var app = angular.module('App.home');
    app.factory('showMsg', ['$q', '$timeout',
        function($q, $timeout) {
            function showMsg(msg, type) {
                var className = "";
                if (type === 'info') {
                    className = "teal";
                } else if (type === "alert") {
                    className = "red";
                } else if (type === "note") {
                    className = "yellow";
                }
                var defer = $q.defer();
                var h = document.getElementById('msg');
                h.setAttribute('class', className);
                h.innerHTML = msg;
                var modal = $('#msgModal').modal('show');
                $timeout((function(modal) {
                    return function() {
                        modal.modal('hide');
                        defer.resolve();
                    };
                })(modal), 2500);
                return defer.promise;
            }
            return showMsg;
        }
    ]);
});
