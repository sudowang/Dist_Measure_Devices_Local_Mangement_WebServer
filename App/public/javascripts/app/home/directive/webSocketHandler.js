define(['./../initial'], function() {
    var app = angular.module('App.home');
    app.directive('webSocketHandler', ['$rootScope',
        function($rootScope) {
            return {
                restrict: 'A',
                link: function(scope, element, iAttrs) {
                    $rootScope.Socket = io.connect('/');

                    $rootScope.Socket.on('connect', function() {
                        console.log("Yes,Connect to Server!");
                    });
                    $rootScope.Socket.on('DistMeasureManage', function(data) {
                        try {
                            data = JSON.parse(data);
                            var fnName = "on_" + data.Command;
                            var fn = scope[fnName];
                            if (fn) {
                                fn(data.Para);
                            }
                        } catch(err) {
                            return;
                        }
                    });


                }
            };
        }
    ])
});
