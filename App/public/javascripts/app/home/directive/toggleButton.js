define(['./../initial'], function() {
    var app = angular.module('App.home');
    app.directive('toggleButton', ['$rootScope', '$location', "$timeout",
        function($rootScope, $location, $timeout) {
            return {
                restrict: 'A',
                scope: {
                    checked: "=",
                    unchecked: "=",
                    checkStr: '@',
                    uncheckStr: "@"
                },
                templateUrl: 'partials/toggleButton.html',
                link: function(scope, element, iAttrs) {
                    scope.check = true;
                    var el = element.find('.toggle-button');
                    scope.toggle = function() {
                        scope.check = !scope.check;
                        if (scope.check && scope.checked) {
                            $timeout(function(arguments) {
                                scope.checked();
                            }, 200);
                        } else if (!scope.check && scope.unchecked) {
                            $timeout(function(arguments) {
                                scope.unchecked();
                            }, 200);

                        }
                    };
                }
            };
        }
    ])
});
