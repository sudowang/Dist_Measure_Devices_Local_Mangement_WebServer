define(['./../initial'], function() {
    var app = angular.module('App.home');
    app.directive('digitInput', ['$rootScope', '$location',
        function($rootScope, $location) {
            return {
                restrict: 'A',
                scope: {
                    
                },
                templateUrl: 'partials/digitInput.html',
                link: function(scope, element, iAttrs) {
                    if (!scope.digit) {
                        scope.digit = 33;
                    }
                }
            };
        }
    ])
});
