define(['./../initial'], function() {
    var app = angular.module('App.home');
    app.factory('Data', ['$filter',
        function($filter) {
            var Data = function(para) {
                if (para) {
                    this.Device = para.Device;
                    this.Value = para.Value;
                    this.Time = $filter('date')(new Date(), 'HH:mm:ss');
                } else {
                    this.Device = "";
                    this.Value = "________";
                    this.Time = "________";
                }
            };
            return Data;
        }
    ]);
});
