define([], function() {
    'use strict';
    var app = angular.module('App.routes', ['ngRoute']);
    app.config(['$routeProvider', '$locationProvider',
        function($routeProvider, $locationProvider) {
            $routeProvider.when('/', {
                templateUrl: 'partials/home.html',
                controller: 'homeCtrl'
            });
            
            $routeProvider.otherwise({
                redirectTo: '/'
            });
            $locationProvider.html5Mode(true);
        }
    ]);

});
