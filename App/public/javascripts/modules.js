define(['app/home/index'], function() {
    angular.module('App.modules', [
        'App.home'
        //modules are defined under 'javascripts/app/xxxx' folder
        //each folder contains an 'index.js' file as entry point.
    ]);
});
