define([
    'modules',
    'routes'
], function() {
    'use strict';
    angular.module('App', [
        'App.modules',
        'App.routes'
    ]);
});
