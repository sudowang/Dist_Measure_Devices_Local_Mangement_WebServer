 require.config({
     baseUrl: 'javascripts',
     paths: {
         jquery: 'bin/jquery/jquery',
         semantic: 'bin/semantic/semantic',
         highcharts: 'bin/highcharts/highcharts'
     },
     shim: {
         'jquery': {
             'exports': 'jquery'
         },
         'semantic': ['jquery'],
         'highcharts': ['jquery']
     }
 });

 require(['app', 'jquery', 'semantic', 'highcharts'], function() {
     'use strict';
     //手动启动app
     var htmlElement = document.getElementsByTagName("html")[0];
     var $html = angular.element(htmlElement);
     $html.attr("data-ng-app", "App");
     angular.element().ready(function() {
         angular.bootstrap($html, ["App"]);
     });
 });
